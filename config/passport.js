require("dotenv").config();
const passport = require("passport");
const StravaStrategy = require("passport-strava-oauth2").Strategy;
const UsersModel = require("../models/Accounts");

const STRAVA_CLIENT_ID = process.env.STRAVA_CLIENT_ID;
const STRAVA_CLIENT_SECRET = process.env.STRAVA_CLIENT_SECRET;
const callbackURL = process.env.callbackURL;

passport.use(
  new StravaStrategy(
    {
      clientID: STRAVA_CLIENT_ID,
      clientSecret: STRAVA_CLIENT_SECRET,
      callbackURL: callbackURL,
    },
    function (accessToken, refreshToken, profile, done) {
      if (profile) {
        const { provider, id, displayName } = profile;
        UsersModel.findOne({ id })
          .then((result) => {
            if (result) {
              return done(null, result);
            }
            const user = new UsersModel({
              accessToken,
              refreshToken,
              provider,
              id,
              name: displayName,
            });
            user
              .save()
              .then((result) => {
                return done(null, result);
              })
              .catch((e) => {
                console.log(e);
              });
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  )
);

passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

module.exports = passport;
