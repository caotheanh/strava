const Joi = require("joi");
const ActivitieSchema = Joi.object({
  name: Joi.string().required(),
  type: Joi.string().required(),
  startDateLocal: Joi.string().required(),
  elapsedTime: Joi.number().required(),
  description: Joi.string().required(),
  distance: Joi.number().required(),
  trainer: Joi.number().required(),
  commute: Joi.number().required(),
});

module.exports = ActivitieSchema;
