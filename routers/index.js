const express = require("express");
const router = express.Router();
const passport = require("../config/passport");
const activitieController = require("../controllers/activitie.controller");

router.get("/", function (req, res) {
  res.render("index", { user: req.user });
});

router.get("/login", function (req, res) {
  res.render("login");
});

router.get("/auth/strava", passport.authenticate("strava", { session: true }));

router.get(
  "/auth/strava/callback",
  passport.authenticate("strava", {
    failureRedirect: "/login",
    successRedirect: "/",
  })
);

router.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/login");
});

router.post(
  "/activities",
  activitieController.create
);

module.exports = router;
