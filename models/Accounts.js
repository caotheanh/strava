const mongoose = require("mongoose");
const { Schema } = mongoose;

const AccountSchema = new Schema(
  {
    id: Number,
    name: String,
    provider: String,
    accessToken: String,
    refreshToken: String
  },
  {
    timestamps: true,
  }
);

const Accounts = mongoose.model("Accounts", AccountSchema);

module.exports = Accounts;
