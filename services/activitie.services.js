const axios = require("axios");
const { createActivity } = require("../config/api");
module.exports.create = async (values, token) => {
  console.log(values, token);
  try {
    const result = await axios.post(createActivity, values, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return result
  } catch (error) {
      throw new Error(`${error}`)
  }
};
