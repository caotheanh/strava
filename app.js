require("dotenv").config();
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const cookieSession = require("cookie-session");
const expressSession = require("express-session");
const mongoose = require("mongoose");
const mainRouters = require("./routers/index");
const passport = require("./config/passport");
const app = express();

app.use(cors());
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static("public"));
app.use(cors());
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: ["key-one"],
  })
);
app.use(
  expressSession({ secret: "strava", resave: true, saveUninitialized: true })
);
app.use(passport.initialize());
app.use(passport.session());
app.use("/", mainRouters);
mongoose.connect(`${process.env.MongoDBUri}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.listen(3000);

module.exports = app;
