const activitieService = require("../services/activitie.services");
const Joi = require("joi");
const ActivitieSchema = require("../validations/activities");
module.exports.create = async (req, res) => {
  const {
    name,
    type,
    startDateLocal,
    elapsedTime,
    description,
    distance,
    trainer,
    commute,
  } = req.body;
  const elapsedTimeConvert = new Date(`${elapsedTime}`);
  const result = ActivitieSchema.validate(
    {
      name,
      type,
      startDateLocal,
      elapsedTime: elapsedTimeConvert.toISOString(),
      description,
      distance,
      trainer,
      commute,
    }
  );
  if (!result) return res.render("404");
  const { accessToken } = req.user;
  const data = await activitieService.create(
    {
      name,
      type,
      startDateLocal,
      elapsedTime: elapsedTimeConvert.toISOString(),
      description,
      distance,
      trainer,
      commute,
    },
    accessToken
  );
  if (data) {
    return res.render("listActivities", { values: data });
  }
  return res.render("404");
};
